### TODO
1. [Activation functions](https://keras.io/activations/)  

  Try different values, _relu_ seems to work fine while _tanh_ is the only non-linear usable one.
2. Loss functions
3. Evaluate good result for loss, i.e. good number for MSE
4. Implement (de)noising
5. Investigate tied weights
6. Investigate Dropout implementation
7. Investigate greedy-wise layer pre-training
8. [Imbalanced Learning](http://contrib.scikit-learn.org/imbalanced-learn/stable/)

  Like the Oregon paper, because cancer cells are mixed with healthy ones in the dataset.
9. [Gene Enrichment](https://github.com/BioNinja/GSEApy)

### Daily:  
1. [Confusion matrix](http://pandas-ml.readthedocs.io/en/stable/conf_mat.html)

### Extra:
If the data doesn't fit into memory:  
```
x = Input(batch_shape=(batch_size, original_dim))
```
[To retrieve the last layer of the model:](https://keras.io/getting-started/faq/#how-can-i-obtain-the-output-of-an-intermediate-layer)
```   
decoder_layer = autoencoder.layers[-1]
```

### Documentation:
1. [Model page](https://keras.io/models/about-keras-models/)
2. [Functional API](https://keras.io/models/model/)
3. [Thread usage](https://github.com/tensorflow/tensorflow/blob/r1.4/tensorflow/core/protobuf/config.proto)
4. [Splice Autoencoder](https://stackoverflow.com/questions/44472693/how-to-decode-encoded-data-from-deep-autoencoder-in-keras-unclarity-in-tutorial)
5. [TCGA Barcode](https://wiki.nci.nih.gov/display/TCGA/TCGA+barcode)
6. Saving objects
  * [Model (HDF5)](https://keras.io/getting-started/faq/#how-can-i-save-a-keras-model)
  * [History (pickle)](https://pythontips.com/2013/08/02/what-is-pickle-in-python/)
7. [Batch size](https://stackoverflow.com/a/39808784)
8. [Fast ranking](https://stackoverflow.com/questions/6618515/sorting-list-based-on-values-from-another-list)
